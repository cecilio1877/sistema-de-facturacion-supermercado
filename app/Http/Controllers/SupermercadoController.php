<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Producto;

session_start();

class SupermercadoController extends Controller
{

    public function index()
    {
        $productos = Producto::all();
        return view('/supermercado', compact('productos'));
    }

    public function create(Request $request)  //la función create no llevaba Request $request -> revisar crud para ver orden correcto
    {
        if(isset($_SESSION['CARRITO'])) 
        {
            $idProd = array_column($_SESSION['CARRITO'], "id");
        }

        $producto = array(
            'id' => $request->input('id'),
            'nombre' => $request->input('nombre'),
            'precio' => $request->input('precio'),
            'cantidad' => $request->input('cantidad'),
        );


        if(!isset($_SESSION['CARRITO'])) 
        {
            $_SESSION['CARRITO'][0] = $producto;
            return back()->with('msj', 'Producto añadido!');
        } 
        else 
        {
            if (in_array($producto['id'], $idProd))
            {
                $key = \array_search($producto['id'], $idProd);
                $_SESSION['CARRITO'][$key]['cantidad']  += $producto['cantidad'];
                return back()->with('msj', 'El item ha sido actualizado!');
            }   
            else
            {
                $items = count($_SESSION['CARRITO']);
                $_SESSION['CARRITO'][$items] = $producto; 
                return back()->with('msj', 'Producto añadido!');
            }
        }
    }

    public function store(Request $request)
    {

    }

    public function show(Request $request)
    {
        $nombre = $request->input('search');
        $productos = Producto::where('nombre', 'LIKE', "%$nombre%")->get();
        return view('/supermercado', compact('productos'));
    }

    public function edit($id)
    {
        $producto = Producto::find($id);
        return view('/productos/productoEditar', compact('producto'));
    }

    /**
     * Remove the specified resource from storage.
     * @param  int  $id
     */
    
    public function destroy($id)
    {
        $producto = Producto::find($id);
        $producto->delete();
        return redirect()->action('ProductoController@index', ['success' => 'Producto Eliminado.']);
    }

}
