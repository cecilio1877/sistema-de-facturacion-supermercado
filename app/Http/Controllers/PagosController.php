<?php

namespace App\Http\Controllers;

use Auth;
use PDF;
use App\User;
use App\Producto;
use Illuminate\Http\Request;

session_start();

class PagosController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $user = Auth::user();
        return view('/pagos', compact('user'));
    }

    /**
     * Creación Factura
     */
    public function factura()
    {
        $user = Auth::user();
        $telefono = session('telefono');
        $pdf = PDF::loadView('factura', compact('user', 'telefono'));
        return $pdf->stream();
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        foreach ($_SESSION['CARRITO'] as $item) 
        {
            $producto = Producto::find($item['id']);

            if($producto->stock < $item['cantidad'])
                return redirect ('/supermercado')->with('msj', 'Producto Agotado!');
            else 
            {
                $producto->stock -=  $item['cantidad'];
                $producto->save();
            }
        }

        session(['telefono' => $request->input('iTelefono')]);
        session(['flag' => true]);
        
        return redirect ('/factura');
    }

}
