<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Producto;

class ProductoController extends Controller
{

    public function index()
    {
        $productos = Producto::all();
        return view('/productos/productoIndex', compact('productos'));
        //return response()->json($productos, 201);
    }

    public function create()
    {
        return view('/productos/productoCrear');
    }

    public function store(Request $request)
    {
        Producto::create([
            'nombre' => $request->input('iNombre'),
            'precio' => $request->input('iPrecio'),
            'stock' => $request->input('iStock'),
            'imagen' => $request->input('iImagen'),
        ]);
        return redirect('productos');
    }

    public function show($id)
    {
        $producto = Producto::find($id);
        return view('/productos/productoVer', compact('producto'));
        //return response()->json($producto, 201);
    }

    public function edit($id)
    {
        $producto = Producto::find($id);
        return view('/productos/productoEditar', compact('producto'));
    }


    public function update(Request $request, $id)
    {
        $producto = Producto::find($id);
        $producto->nombre = $request->input('iNombre');
        $producto->precio = $request->input('iPrecio');
        $producto->stock = $request->input('iStock');
        $producto->imagen = $request->input('iImagen');
        $producto->save();
        return redirect('productos');
    }

    /**
     * Remove the specified resource from storage.
     * @param  int  $id
     */
    
    public function destroy($id)
    {
        $producto = Producto::find($id);
        $producto->delete();
        return redirect()->action('ProductoController@index', ['success' => 'Producto Eliminado.']);
    }

    /*Busqueda por nombre*/

    public function search($nombre)
    {
        $productos = Producto::all()->where('nombre', $nombre);
        return view('/productos/productoIndex', compact('productos'));
        //return response()->json($productos, 201);
    }
}
