<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/promociones', function () {
    return view('promociones');
});

Route::get('/about', function () {
    return view('about');
});


// RUTAS PARA PRODUCTOS
Route::get('/productos', 'ProductoController@index');
Route::get('/productos/Crear', 'ProductoController@create');
Route::get('/productos/Guardar', 'ProductoController@store');
Route::get('/productos/Ver/{CODIGO}', 'ProductoController@show');
Route::get('/productos/Editar/{CODIGO}', 'ProductoController@edit');
Route::get('/productos/Actualizar/{CODIGO}', 'ProductoController@update');
Route::get('/productos/Eliminar/{CODIGO}', 'ProductoController@destroy');


// RUTAS PARA SUPERMERCADO
Route::get('/supermercado', 'SupermercadoController@index');
Route::post('/supermercado/Crear', 'SupermercadoController@create');
Route::get('/supermercado/Buscar', 'SupermercadoController@show');

// RUTAS PARA COMPRAS
Route::get('/compras', 'ComprasController@index');
Route::post('/compras/Eliminar/{CODIGO}', 'ComprasController@destroy');
Route::get('/pagoRealizado', 'ComprasController@show');

//RUTAS PARA PAGOS
Route::get('/pagos', 'PagosController@index');
Route::post('/pagos/Guardar', 'PagosController@store');
Route::get('/factura', 'PagosController@factura');