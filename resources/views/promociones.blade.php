@extends('layouts.app')

@section('content')

<div class="container">
    
        <div class="panel-group" id="acordion" style="padding-top:20px; padding-bottom:20px">           
            <div class="panel panel-default">
                <div class="panel-heading">
                <h4 class="panel-title">
                     <a data-toggle="collapse" data-parent="#accordion" href="#collapse1">Panadería</a>
                 </h4>
                 </div>


                <div id="collapse1" class="panel-collapse collapse in">

                <center><img src="{{ asset('images/promo-pan.jpg') }}" ><br></center>

                    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif

                    <p>*Las promociones aplican según productos pagando en efectivo.</p>
                    <p>DÍA DEL PAN: Aplica en panes de sal, dulce e integrales de ($2.00, $2.20, $3.00, $3.20, $5.00 y $5.20).</p>
                </div>
                
            </div>

            <div class="panel panel-default">
                <div class="panel-heading">
                <h4 class="panel-title">
                     <a data-toggle="collapse" data-parent="#accordion" href="#collapse2">Viveres</a>
                 </h4>
                 </div>


                <div id="collapse2" class="panel-collapse collapse in">

                <center><img src="{{ asset('images/promo-huevos.jpg') }}"><br></center>

                    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif

                    <p>¡EXTRA EXTRA! Llegó la súper promoción de huevos. Aplica para más de 60 unidades.</p>
                </div>
                
            </div>
        </div>
    
</div>

@endsection