@extends('layouts.app')

@section('content')

<div class="container" style="position: relative; top:-88px; margin-bottom:50px">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading"><strong>Acerca de...</strong></div>
                <div align="center" style="margin-top:10px">
                    <p>Bryan Morales</p>
                    <p>Haydeé Ninahualpa</p>
                    <p>Javier Zuñiga</p>
                </div>
                <div class="panel-body">

                    <img src="{{ asset('images/teamgroup.jpeg') }}"><br>

                    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif

                    Contáctanos!
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
