@extends('layouts.app')

@section('content')

<!-- Compiled and minified CSS -->
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/css/materialize.min.css">
<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

@auth
    <div id="test1" class="col s12">
      <div class="container section">
        <div class="row">
          <h3 class="header">Productos</h3><br>
          <div class="col s9">
            <a class="waves-effect waves-light blue darken-4 btn" href="/productos/Crear" style="color: white"><i class="material-icons left">add</i>Añadir Registro</a>
          </div>
          <br><br><br>
          <table id="myTable" class="table table-hover ">
            <tr>
              <th colspan="9" class="white-text blue darken-3"><center>PRODUCTOS</center></th>
            </tr>
            <tr class="white-text blue darken-2">
              <td><center>
                Seleccionar
              </td>
              <td><center>Ver</td>
              <td><center>Borrar</td>
              <td><center>Editar</td>
              <td><center>Código</center></td>
              <td><center>Nombre</center></td>
              <td><center>Precio</center></td>
              <td><center>Stock</center></td>
            </tr>
            @foreach ($productos as $prod)
            <tr>
              <td>
                <center>
                  <label>
                    <input type="checkbox" />
                    <span></span>
                  </label>
                </center>
              </td>
                <center>
                  <td><center><form action="/productos/Ver/{{$prod->id}}" method="GET">
                     <button class="waves-effect waves-light blue darken-4 btn"><i class="material-icons center">pageview</i></button>
                    </form>
                  </td>
                  <td><center><form action="/productos/Eliminar/{{$prod->id}}" method="GET" onsubmit="return ConfirmDelete()">
                   <button class="waves-effect waves-light blue darken-4 btn"><i class="material-icons center">delete</i></button>
                  </form>
                  </td>
                  <td><center><form action="/productos/Editar/{{$prod->id}}" method="GET">
                  <button class="waves-effect waves-light blue darken-4 btn"><i class="material-icons center">edit</i></button>
                  </form>
                </td>
                </center>

              <td><center> {{$prod->id}} </center></td>
              <td><center>{{$prod->nombre}}</center></td>
              <td><center>{{$prod->precio}}</center></td>
              <td><center>{{$prod->stock}}</center></td>
            </tr>
            @endforeach
          </table>
        </div>
      </div>
    </div>
    <script>
        $(document).ready(function () {
          $("#alert_close").click(function()
          { var $toastContent = $('<span>I am toast content</span>');
            Materialize.toast($toastContent, 5000);
          });

            $('#myTable').DataTable({
                "paging": false,
                "info": false,
                "autoWidth": true,
                "searching": true, // Search box and search function will be actived
                "dom": '<"top"f>rt<"bottom"ilp><"clear">',
                "language": {
                    "zeroRecords": "No existe registros",
                    "infoEmpty": "No se econtró ningún registro",
                    "sSearch": "Buscar:   "
                        }
            });

        });
    function ConfirmDelete()
    {
    var x = confirm("¿Está seguro que desea eliminar este producto?");
    if (x)
        return true;
    else
        return false;
    }
</script>

@endauth
@endsection