<!-- Compiled and minified CSS -->
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/css/materialize.min.css">
<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

    <!-- Normal Menu-->
    <div id="test2" class="col s12">
        <div class="container section">
          <div class="row">
            <form method="GET" action ="/productos/Actualizar/{{$producto->id}}">
               {{ csrf_field() }}
              <h2 class="header">Productos</h2>
              <h3 class="header">Editar</h3>
              <div class="col s12">

                <div class="form-group">
                  <input placeholder="Nombre del producto" name="iNombre"  value="{{$producto->nombre}}" id="iNombre" type="text" class="validate"style="color: black;" required>
                  <label for="first_name">Nombre</label>
                </div>

                <div class="form-group">
                  <input placeholder="Costo del producto" name="iPrecio"  id="iPrecio" value="{{$producto->precio}}" type="number" class="validate" style="color: black;" min="0" step="0.01" required>
                  <label for="first_name">Precio</label>
                </div>

                <div class="form-group">
                  <input placeholder="Cantidad disponible"name="iStock"  value="{{$producto->stock}}" id="iStock" type="number" class="validate" style="color: black;" min="0" required>
                  <label for="first_name">Stock</label>
                </div>

                <div class="form-group">
                  <input placeholder="Url Imagen" name="iImagen" id="iImagen" type="url"><br>
                  <label for="first_name">Imagen</label>
                </div>

                <br><label for="first_name">* Campo Requerido</label><br><br>

              </div>

              <button type="submit" class="waves-effect waves-light green btn" href="/productos/Actualizar/{{$producto->id}}"><i class="material-icons left">check</i>Guardar</button>
              <a class="waves-effect waves-light blue darken-4 btn" href="/productos"><i class="material-icons left">cancel</i>Cancelar</a>
            </form> 
          </div>
        </div>
      </div>