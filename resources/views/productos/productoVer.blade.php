<!-- Compiled and minified CSS -->
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/css/materialize.min.css">
<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

    <!-- Normal Menu-->
    <div id="test2" class="col s12">
        <div class="container section">
          <div class="row">
               {{ csrf_field() }}
              <h2 class="header">Productos</h2>
              <h3 class="header">Ver</h3>
              <div class="col s12">
                <div class="form-group">
                    <input placeholder="Código del Producto" name="codigobanco" id="codigobanco" value="{{$producto->id}}" style="color: black;"type="text" required class="validate" maxlength="15" disabled> 
                    <label for="first_name">Código</label>
                </div>

                <div class="form-group">
                  <input placeholder="Nombre" name="nombrebanco"  value="{{$producto->nombre}}" id="nombrebanco" type="text" class="validate"disabled style="color: black;">
                  <label for="first_name">Nombre</label>
                </div>

                <div class="form-group">
                  <input placeholder="Pais" name="paisbanco"  id="paisbanco" value="{{$producto->precio}}" type="text" class="validate" disabled style="color: black;">
                  <label for="first_name">Precio</label>
                </div>

                <div class="form-group">
                  <input placeholder="Categoría"name="categoriabanco"  value="{{$producto->stock}}" id="categoriabanco" type="text" class="validate"disabled style="color: black;">
                  <label for="first_name">Stock</label>
                </div>
                <br>
                <br>
              </div>
              <a class="waves-effect waves-light blue darken-4 btn" href="/productos"><i class="material-icons left">cancel</i>Volver</a>
          </div>
        </div>
      </div>