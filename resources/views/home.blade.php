@extends('layouts.app')

@section('content')

@auth

<div class="container">

    @if(session()->has('msj'))
        <div class="alert alert-info alert-dismissible show" role="alert" style="position: relative; top:-90px; 
        margin-left: 930px; margin-right: 40px; margin-bottom: -45px; right: 64px;">
            <strong>{{ session('msj') }}
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span> </button>
        </div>
    @endif

    <div class="row" style="padding-top:25px; padding-bottom:25px">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Bienvenido!</div>

                <div class="panel-body">

                    <img src="{{ asset('images/imagen-inicio.jpg') }}"><br>

                    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif

                    Usted ha iniciado sesión!
                </div>
            </div>
        </div>
    </div>
</div>

@endauth
@endsection
