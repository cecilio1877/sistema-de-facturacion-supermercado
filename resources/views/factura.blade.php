<!-- Styles -->
<link href="css/app.css" rel="stylesheet">

<div style="height:20px; background-color:#0f75bc" />
<div class="container" style="padding-top:20px; background-color:white; padding-bottom:20px">

<label for="" style="font-size:28px; color:black;">FACTURA</label>

<img src="images/logo.png" style="height:75px; width:75px; position:absolute; right:40; top:60"><br>

<div style="margin-top:100px">

    <div style="color:black; margin-top:10px; margin-bottom:25px">
        <table width="100%">
            <thead style="margin-top:10px; margin-bottom:15px">
                <tr>
                    <th width="60%" style="border: hidden; font-size:21px;">JAH Supermercado</th>
                    <th width="40%" style="border: hidden; font-size:21px;">{{ $user->name }} {{ $user->apellido}}</th>
                </tr>
            </thead>
            <tbody>
                <tr height="110%">
                    <td style="border: hidden">jahsupermercado@email.com</td>
                    <td style="border: hidden">{{ $user->cedula }}</td>
                </tr>
                <tr height="110%">
                    <td style="border: hidden">Av. 12 de Octubre</td>
                    <td style="border: hidden">{{ $user->direccion }}</td>
                </tr>
                <tr height="110%">
                    <td style="border: hidden">P: (+593) 987 654 321</td>
                    <td style="border: hidden">{{ $user->email }}</td>
                </tr>
            </tbody>
        </table>
    </div>

    <div style="height:2px; background-color:gray" />

    <div style="margin-top:25px; margin-bottom:20px"">
        <label for="" style="font-size:15px; color:black;">Número:</label>
        <label for="" style="font-size:15px;">NF0001</label><br>
        <label for="" style="font-size:15px; color:black;">Fecha:</label>
        <label for="" style="font-size:15px;">                        
            @php 
                $hoy = date('d-m-Y');
                echo $hoy;
            @endphp
        </label>
    </div>
    
    <div>
        <?php if(!empty($_SESSION['CARRITO'])) { ?>
        <table class="table table-light" >

            <thead style="background-color:#1765d4; color:white;">
                <tr>
                    <th width="45%">Descripción</th>
                    <th width="15%">Cantidad</th>
                    <th width="20%">Precio</th>
                    <th width="20%">Total</th>
                </tr>
            </thead>
            <tbody>

            <?php $total=0; foreach ($_SESSION['CARRITO'] as $key => $producto) { ?>
                <tr>
                    <td width="45%">{{ $producto['nombre'] }}</td>
                    <td width="15%">{{ $producto['cantidad'] }}</td>
                    <td width="20%">$ {{ $producto['precio'] }}</td>
                    <td width="20%">$ {{ number_format($producto['precio'] * $producto['cantidad'], 2)}}</td>
                </tr>
            <?php $total += ($producto['precio'] * $producto['cantidad']); } ?>
                <tr>
                    <td></td><td></td>
                    <td><strong>Subtotal</td>
                    <td><strong>$ <?php echo number_format($total,2); ?></td>
                </tr>
                <tr>
                    <td></td><td></td>
                    <td><strong>IVA (12%)</td>
                    <td><strong>$ <?php echo number_format($total*0.12,2); ?></td>
                </tr>
                <tr>
                    <td></td><td></td>
                    <td><strong>TOTAL</td>
                    <td><strong>$ <?php echo number_format($total*1.12,2); ?></td>
                </tr>
            </tbody>

        </table><br>
        <?php } ?>
    </div>           

</div>