@extends('layouts.app')

@section('content')

@auth

@if(session()->has('msj'))
    @if(session('msj') == "Producto añadido!")
        <div class="alert alert-success alert-dismissible show" role="alert" style="position: relative; top:-90px; 
        margin-left: 1125px; margin-right: 40px; margin-bottom: -45px; right: 64px;">
            <strong>{{ session('msj') }}
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span> </button>
        </div>
    @else
        <div class="alert alert-warning alert-dismissible" role="alert" style="position: relative; top:-90px; 
        margin-left: 1050px; margin-right: 40px; margin-bottom: -45px; right: 64px;">
            <strong>{{ session('msj') }}
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span> </button>
        </div>
    @endif
@endif

<div class="container" style="padding-bottom:25px">

    <br><center><div class="container-1">
    <form method="GET" action="/supermercado/Buscar">
        <span class="icon"><button type="submit"><i class="fa fa-search"></i></button></span>
        <input style="color:black" type="search" id="search" name="search" placeholder="Buscar..." />
    </form>
    </div></center><br><br><br><br>

    <div class="row">
    
        @foreach ($productos as $prod)
        
        <div class="col-md-4">
        
            <div class="panel panel-default">

                <div class="panel-heading">{{$prod->nombre}}</div>

                <div class="panel-body">
                 
                    <img src="{{ $prod->imagen }}" style="height:300px; width:100%"><br>

                    <div class="panel-body">
                        <center>Precio: {{ $prod->precio }}</center>
                    </div>

                </div>
               
                
                <div class="panel-footer">
                    <table>
                        <tr style="background-color: #f5f5f5">
                            <td style="color: #000000" align="right">Cantidad:</td>

                            <td><form action="/supermercado/Crear" method="POST">
                            {{ csrf_field() }}

                                <input style="border-radius:6px; width:45px; margin-right:55px" type="number" 
                                    id="cantidad" name="cantidad" value="1" min="1" max="100">
                                <input type="hidden" name="id" id="id" value={{ $prod->id }}>
                                <input type="hidden" name="nombre" id="nombre" value={{ $prod->nombre }}>
                                <input type="hidden" name="precio" id="precio" value={{ $prod->precio }}>

                                <button type="subtmit" name="btnAdd" value="Add" style="color: #1765d4; border:none">
                                    <i class="fas fa-cart-plus fa-lg"></i>
                                </button>

                            </form></td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
        
        @endforeach

    </div>   
</div>

@endauth
@endsection