<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('Proyecto', 'JAH Supermercado') }}</title>

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <script src="https://kit.fontawesome.com/9c3e5d6670.js" crossorigin="anonymous"></script> <!--Iconos-->
    <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Open+Sans">
    <link href="{{ asset('css/buscador.css') }}" rel="stylesheet">
    <link href="{{ asset('css/navbar.css') }}" rel="stylesheet">
</head>
<body>
    <div id="app">
        <nav class="navbar navbar-default navbar-static-top" style="background-color:#1765d4; color:white">
            <div class="container">
                <div class="navbar-header">

                    <!-- Collapsed Hamburger -->
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#app-navbar-collapse" aria-expanded="false">
                        <span class="sr-only">Toggle Navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>

                    <!-- Branding Image -->
                    <a class="navbar-brand" href="{{ url('/') }}"  style="color:white">
                        {{ config('Proyecto', 'JAH Supermercado') }}
                    </a>
                </div>

                <div class="collapse navbar-collapse" id="app-navbar-collapse">
                    <!-- Left Side Of Navbar -->
                    <ul class="nav navbar-nav">
                        @guest   
                        @else

                            @if(@Auth::user()->rol == 'user')
                                <li><a href="/home" style="margin-left:120px; color:white">Inicio</a></li>
                                <li><a href="/promociones" style="color:white">Promociones</a></li>
                                <li><a href="/compras" style="color:white;"><strong>Compras</strong></a></li>
                                <li><a href="/supermercado" style="color:white">Supermercado</a></li>
                                
                                <li class="dropdown">
                                    <a  style="color:white" href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false" aria-haspopup="true" v-pre>
                                        Ayuda<span class="caret"></span>
                                    </a>

                                    <ul class="dropdown-menu">
                                        <li><a href="/about">Acerca de...</a>
                                        </li>
                                    </ul>
                                </li>
                            @else
                                <li><a href="/productos" style="color:white; margin-left:300px;">Productos</a></li>
                            @endif          

                        @endguest             
                        &nbsp;
                    </ul>

                    <!-- Right Side Of Navbar -->
                    <ul class="nav navbar-nav navbar-right">
                        <!-- Authentication Links -->
                        @guest
                            <li><a href="{{ route('login') }}" style="color:white">Ingresar</a></li>
                            <li><a href="{{ route('register') }}" style="color:white">Crear Cuenta</a></li>
                        @else
                            <li class="dropdown">
                                <a  style="color:white" href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false" aria-haspopup="true" v-pre>
                                    {{ Auth::user()->name }} <span class="caret"></span>
                                </a>

                                <ul class="dropdown-menu">
                                    <li>
                                        <a href="{{ route('logout') }}"
                                            onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                            Cerrar Sesión
                                        </a>

                                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                            {{ csrf_field() }}
                                        </form>
                                    </li>
                                </ul>
                            </li>
                            @if(@Auth::user()->rol == 'user')
                                <li>
                                    <a href="/compras" style="color:white"><i class="fas fa-shopping-cart fa-lg"></i>
                                        <?php echo (empty($_SESSION['CARRITO']))?null:count($_SESSION['CARRITO']); ?>
                                    </a>
                                </li>
                            @endif
                        @endguest
                    </ul>
                </div>
            </div>
        </nav>

        @yield('content')
    </div>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}"></script>
</body>
</html>
