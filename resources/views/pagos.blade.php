<link href="{{ asset('css/app.css') }}" rel="stylesheet">
<link href="{{ asset('css/navbar.css') }}" rel="stylesheet">

@if(session()->has('flag') && session('flag')==true)
@auth

<div class="container">
    <div class="row" style="padding-top:25px; padding-bottom:25px">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading"><center><strong><h2>Pago Realizado!</div>
                <div class="panel-heading">
                    <a class="btn btn-success col-md-12" href="/pagoRealizado" style="color:white;">
                    <strong>Seguir Comprando</a>
                </div>
                <br>
                <div class="panel-body">

                    <img src="{{ asset('images/imagen-inicio.jpg') }}"><br>

                    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif

                </div>
            </div>
        </div>
    </div>
</div>

@endauth
@else

<div class="container" style="padding-top:20px; background-color:white; padding-bottom:20px">
<center><label for="" style="font-size:30px; color:black;">PAGO</label></center>

<form method="POST" action="pagos/Guardar">
    {{ csrf_field() }}
    <label for="" style="font-size:20px; color:black; padding-bottom:7px">Información del cliente</label>
    <div class="form-row">
        <div class="form-group col-md-6">
            <label for="nombre">Nombre</label>
            <input type="text" class="form-control" id="nombre" value="{{ $user->name }}">
        </div>
        <div class="form-group col-md-6">
            <label for="apellido">Apellido</label>
            <input type="text" class="form-control" id="apellido" value="{{ $user->apellido }}">
        </div>
    </div>
    <div class="form-group" style="padding-left:15px; padding-right:15px">
        <label for="correo">E-Mail</label>
        <input type="email" class="form-control" id="correo" value="{{ $user->email }}">
    </div>

    <label for="" style="font-size:20px; color:black; padding-bottom:7px">Datos de Envío</label>
    <div class="form-group" style="padding-left:15px; padding-right:15px">
        <label for="direccion">Dirección</label>
        <input type="text" class="form-control" id="direccion" value="{{ $user->direccion }}" placeholder="Apartamento, estudio, o piso">
    </div>
        <div class="form-group" style="padding-left:15px; padding-right:15px">
        <label for="direccion">Ciudad</label>
        <input type="text" class="form-control" id="direccion" placeholder="Ciudad actual">
    </div>
    <div class="form-row">
        <div class="form-group col-md-6">
        <label for="pais">País</label>
        <input type="text" class="form-control" id="pais">
        </div>
        <div class="form-group col-md-4">
        <label for="provincia">Provincia</label>
        <select id="provincia" class="form-control">
            <option selected>Escoja...</option>
            <option>...</option>
         </select>
        </div>
        <div class="form-group col-md-2">
        <label for="inputZip">Zip</label>
        <input type="text" class="form-control" id="inputZip">
        </div>
    </div>

    <div class="form-group" style="padding-left:15px; padding-right:15px">
        <label for="direccion">Teléfono</label>
        <input type="text" class="form-control" name="iTelefono" id="iTelefono" placeholder="Teléfono [opcional]">
    </div>
    <div class="form-group" style="padding-left:15px; padding-right:15px">
        <div class="form-check">
        <input class="form-check-input" type="checkbox" id="gridCheck">
        <label class="form-check-label" for="gridCheck">
            Estoy de acuerdo con los términos y condiciones
        </label>
        </div>
    </div>
    <div style="padding-left:15px; padding-right:15px; padding-top:7px">
        <button type="submit" class="btn btn-success col-md-12"><strong>Realizar Pago</strong></button><br><br>
        <a class="btn btn-warning col-md-12" href="/compras">Cancelar</a>
    </div>
</form>

</div>

@endif