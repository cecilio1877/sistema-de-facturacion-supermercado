@extends('layouts.app')

@section('content')

@auth 
<div class="container" style="padding-top:15px; padding-bottom:15px" >
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading" align="center"><strong>Carrito</strong></div>
                <div class="panel-body">

                <?php if(!empty($_SESSION['CARRITO'])) { ?>
                <table class="table table-light" >
                    <thead style="background-color:#1765d4; color:white">
                        <tr>
                            <th width="40%">Descripción</th>
                            <th width="15%">Cantidad</th>
                            <th width="20%">Precio</th>
                            <th width="20%">Total</th>
                            <th width="5%">Quitar</th>
                        </tr>
                    </thead>
                    <tbody>

                    <?php $total=0; foreach ($_SESSION['CARRITO'] as $key => $producto) { ?>
                        <tr>
                            <td width="40%">{{ $producto['nombre'] }}</td>
                            <td width="15%">{{ $producto['cantidad'] }}</td>
                            <td width="20%">$ {{ $producto['precio'] }}</td>
                            <td width="20%">$ {{ number_format($producto['precio'] * $producto['cantidad'], 2)}}</td>

                            <td width="5%" align="center">
                                <form action="/compras/Eliminar/{{ $producto['id'] }}" method="POST">
                                {{ csrf_field() }}
                                    <button type="submit" name="btnDelete" value="Delete" style="border:none">
                                        <i class="fas fa-trash fa-lg"></i>  
                                    </button>
                                </form>
                            </td>

                        </tr>
                    <?php $total += ($producto['precio'] * $producto['cantidad']); } ?>
                        <tr>
                            <td></td><td></td>
                            <td><strong>TOTAL</td>
                            <td><strong>$ <?php echo number_format($total,2); ?></td>
                            <td>
                                <form method="GET" action="/pagos">
                                    {{ csrf_field() }}
                                    <button style="border:none; background-color: #2fcbe0; color: white">
                                    <strong>Pagar</strog></button>
                                </form>
                            </td>
                        </tr>
                    </tbody>
                </table>
                <?php } ?><br>

                    <img src="{{ asset('images/imagen-inicio.jpg') }}"><br>
                </div>
            </div>
        </div>
    </div>
</div>

@endauth
@endsection